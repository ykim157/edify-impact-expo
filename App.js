import React from 'react';
import {
  Permissions,
  Notifications,
  Constants,
} from 'expo';
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Platform,
  StatusBar,
  WebView,
} from 'react-native';

let appVersion = require('./app.json').expo.version;

const api_token = 'fe911f3c9343dbaf4f3a876fc7e3519b'

async function registerForPushNotificationsAsync(devicePlatform, installId) {

  const { status: existingStatus } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  );
  let finalStatus = existingStatus;

  // only ask if permissions have not already been determined, because
  // iOS won't necessarily prompt the user a second time.
  if (existingStatus !== 'granted') {
    // Android remote notification permissions are granted during the app
    // install, so this will only ask on iOS
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
  }

  // Stop here if the user did not grant permissions
  if (finalStatus !== 'granted') {
    return;
  }

  // Get the token that uniquely identifies this device
  let push_token = await Notifications.getExpoPushTokenAsync();

  // POST the token to your backend server from where you can retrieve it to send push notifications.
  const Http = new XMLHttpRequest();
  const PUSH_ENDPOINT = `https://edifydb-staging.herokuapp.com/api/impact_data/push_token_reg?token=${api_token}&expo_push_token=${push_token}&device_id=${installId}&device_type=${devicePlatform}&app_version=${appVersion}`;

  Http.open("POST", PUSH_ENDPOINT);
  Http.send();

  Http.onreadystatechange = (e) => {
    console.log(Http.responseText)
    // console.log(PUSH_ENDPOINT)
  }

}

export default class AppContainer extends React.Component {
  state = {
    notification: {},
    phoneNumber: '',
    installId: Constants.installationId,
    devicePlatform: Platform.OS
  };
  
  componentDidMount() {
    registerForPushNotificationsAsync(this.state.devicePlatform, this.state.installId);  
    this._notificationSubscription = Notifications.addListener(this._handleNotification);
  }

  _handleNotification = (notification) => {
    this.setState({notification: notification});
  };

  registerTokens = () => {
    
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor:'white'}}>
        <StatusBar
          backgroundColor= "#3A6F8F"
          barStyle="light-content"
        />
        <WebView
          source={{uri: `https://edifydb-staging.herokuapp.com/datos/?device_id=${this.state.installId}`}} //`https://edifydb-staging.herokuapp.com/datos/?device_id=${this.state.installId}`
          style={{marginTop: 20, flex: 1}}
        />
      </View>

    );
  }
}

const styles = StyleSheet.create({
   input: {
      margin: 15,
      height: 30,
      width: 200,
      borderColor: 'grey',
      borderWidth: 1,
      padding: 4
   },
   submitButton: {
      backgroundColor: 'green',
      padding: 10,
      margin: 15,
      height: 40,
   },
   submitButtonText:{
      color: 'white'
   },
   view:{
     flex: 1, 
     backgroundColor: 'white',
   },
})

//////////////////////////////////////////////////////////////////////////////
// DEPRECATED
//////////////////////////////////////////////////////////////////////////////
//   {/* <Text>Notification Status: {this.state.notification.origin}</Text>
//   <Text>Installation ID: {this.state.installId}</Text>
//   <Text>Device Platform: {this.state.devicePlatform}</Text>
//   <Text>App Version: {appVersion}</Text>
//   <TextInput
//     style={styles.input}
//     placeholder = "Phone number"
//     onChangeText={(phoneNumber) => this.setState({phoneNumber})}
//     value={this.state.phoneNumber}
//   />
//   <TouchableOpacity
//       style = {styles.submitButton}
//       onPress = {
//         () => this.registerTokens(this.state.phoneNumber)
//       }>
//       <Text style = {styles.submitButtonText}> Submit </Text>
//   </TouchableOpacity> */}